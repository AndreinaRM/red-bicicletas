var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:3000/api/bicicletas";

describe("bicicleta API", () => {
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongo conection error: '));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () =>{
        it("Status 200", (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe("POST BICICLETAS /create", () => {
        it("STATUS 200", (done) => {
            var headers = {'Content-Type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "azul", "modelo": "urbano", "lat": -34, "lng": -54 }';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("azul");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });
});

   /* describe("POST BICICLETAS /update", () => {
        it("STATUS 200", (done) => {
            var a = new Bicicleta(10,"azul","urbana",[-34.6012424,-58.3861497]);
            Bicicleta.add(a);
            var headers = {'Content-Type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "verde", "modelo": "urbano", "lat": -34, "lng": -54 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("verde");
                done();
            });
        });
    });

    describe("POST BICICLETAS /delete", () => {
        it("STATUS 200", (done) => {
            var a = new Bicicleta(10,"azul","urbana",[-34.6012424,-58.3861497]);
            Bicicleta.add(a);
            expect(Bicicleta.findById(10).color).toBe("azul");
            var headers = {'Content-Type' : 'application/json'};
            var aBici = '{ "id": 10 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
            });
        });
    });*/
    