const { ConsoleReporter } = require('jasmine');
var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var Usuario = require("../../models/usuario");
var Reserva = require("../../models/reserva");


describe('Testing Usuarios', function(){
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongo conection error: '));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un Usuario reserva una bici', () => {
        it('desde existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Ezequiel'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});

/*beforeEach(() => {Bicicleta.allBicis = [];});

describe("Bicicleta.allBicis", () => {
    it("comienza vacía", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    });
}); 

describe("Bicicleta.add", () => {
    it("agregamos una", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1,"azul","urbana",[-34.6012424,-58.3861497]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
    });
}); 

describe("Bicicleta.findById", () => {
    it("debe devolver la bici con id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var aBici1 = new Bicicleta(1,"azul","urbana",[-34.6012424,-58.3861497]);
    var aBici2 = new Bicicleta(2,"roja","urbana",[-34.6012424,-58.3861497]);
    Bicicleta.add(aBici1);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici1.color);
    expect(targetBici.modelo).toBe(aBici1.modelo);
    });
}); 

describe("Bicicleta.removeById", () => {
    it("debe eliminar con el id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var aBici1 = new Bicicleta(1,"azul","urbana",[-34.6012424,-58.3861497]);
    var aBici2 = new Bicicleta(2,"roja","urbana",[-34.6012424,-58.3861497]);
    Bicicleta.add(aBici1);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici1.color);
    expect(targetBici.modelo).toBe(aBici1.modelo);

    Bicicleta.removeById(1);
    expect(Bicicleta.allBicis.length).toBe(1);
    });
}); */